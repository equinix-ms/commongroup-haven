// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"errors"

	authorizationv1 "k8s.io/api/authorization/v1"
)

func fundamentalSelfTest(config *Config) (Result, error) {
	sar := &authorizationv1.SelfSubjectAccessReview{
		Spec: authorizationv1.SelfSubjectAccessReviewSpec{
			NonResourceAttributes: &authorizationv1.NonResourceAttributes{
				Verb: "*",
				Path: "*",
			},
		},
	}

	response, err := config.Kube.AccessReview().Create(sar)

	if err != nil {
		return ResultNo, err
	}

	// This check is fundamental. If it fails execution must stop.
	if !response.Status.Allowed {
		return ResultNo, errors.New("HCC does not have cluster-admin access.")
	}

	return ResultYes, nil
}
