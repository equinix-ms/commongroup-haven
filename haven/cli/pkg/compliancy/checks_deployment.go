// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"strings"

	apiv1 "k8s.io/api/core/v1"
)

func deploymentLogAggregation(config *Config) (Result, error) {
	pods, err := config.Kube.Pods().List(apiv1.NamespaceAll)
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range whitelist.Loggers {
			if strings.Contains(p.Name, l.Name) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentMetricsServer(config *Config) (Result, error) {
	pods, err := config.Kube.Pods().List(apiv1.NamespaceAll)
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, m := range whitelist.Metrics {
			if strings.Contains(p.Name, m.Name) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentHttpsCerts(config *Config) (Result, error) {
	pods, err := config.Kube.Pods().List(apiv1.NamespaceAll)
	if err != nil {
		return ResultNo, err
	}

	// Detection via Pods.
	for _, pod := range pods.Items {
		for _, p := range whitelist.CertPods {
			if strings.Contains(pod.Name, p.Name) {
				return ResultYes, nil
			}
		}
	}

	crds, err := config.Kube.CustomResourceDefinitions().List()
	if err != nil {
		return ResultNo, err
	}

	// Detection via CRD's.
	for _, crd := range crds.Items {
		for _, c := range whitelist.CertCrds {
			if strings.Contains(crd.Spec.Names.Kind, c.Name) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

// suggestionDeploymentHavenDashboard is a 'Suggested Check' meaning it's not counted towards Haven Compliancy.
func suggestionDeploymentHavenDashboard(config *Config) (Result, error) {
	list, err := config.Kube.Deployments().List(apiv1.NamespaceAll)

	if err != nil {
		return ResultNo, err
	}

	for _, d := range list.Items {
		if "haven-dashboard" == d.Name {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}
