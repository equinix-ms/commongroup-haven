// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// RunCommandOnPrivilegedPod takes a string as command input and returns the output from
// running that command from a privileged pod, unless this results in an error.
// It's possible to force scheduling of the pod on a master node when needed.
func RunCommandOnPrivilegedPod(config *Config, useMaster bool, failOnExecErr bool, cmd string) (string, error) {
	// Find a master node to work with.
	nodes, err := config.Kube.Nodes().List()
	if err != nil {
		return "", err
	}

	var master apiv1.Node
	if useMaster {
		for _, n := range nodes.Items {
			if n.Labels["kubernetes.io/role"] == "master" {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/control-plane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/controlplane"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node-role.kubernetes.io/master"]; exists {
				master = n
				break
			}

			if _, exists := n.Labels["node.kubernetes.io/master"]; exists {
				master = n
				break
			}
		}

		if master.Name == "" {
			return "", errors.New("Could not find any master node to run privileged pod on")
		}
	}

	// Create privileged pod. See https://miminar.fedorapeople.org/_preview/openshift-enterprise/registry-redeploy/go_client/executing_remote_processes.html.
	app := "hcc-test-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])

	var priv bool = true
	var tolerations []apiv1.Toleration

	if useMaster {
		for _, taint := range master.Spec.Taints {
			tolerations = append(tolerations, apiv1.Toleration{
				Key:      taint.Key,
				Operator: apiv1.TolerationOpExists,
				Effect:   taint.Effect,
			})
		}
	}

	podConfig := &apiv1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: app,
		},
		Spec: apiv1.PodSpec{
			Containers: []apiv1.Container{
				{
					Name:    app,
					Image:   "busybox",
					Command: []string{"cat"},
					Stdin:   true,
					SecurityContext: &apiv1.SecurityContext{
						Privileged: &priv,
					},
				},
			},
			HostPID:     true,
			NodeName:    master.Name,
			Tolerations: tolerations,
		},
	}

	pod, err := config.Kube.Pods().Create(config.Namespace, podConfig)
	if err != nil {
		return "", err
	}

	defer func() {
		if err := config.Kube.Pods().Delete(pod.Namespace, pod.Name); err != nil {
			logging.Error("Could not cleanup privileged pod: '%s'", err.Error())
		}
	}()

	// Wait for 60sec for pod to become ready.
	ticker := time.NewTicker(config.GlobalInterval)
	timeout := time.After(60 * time.Second)
	var podHealthy bool

	for {
		select {
		case <-ticker.C:
			po, err := config.Kube.Pods().Get(config.Namespace, pod.Name)
			if err != nil {
				continue
			}

			if po.Status.Phase != apiv1.PodRunning {
				continue
			}

			podHealthy = true
			ticker.Stop()

		case <-timeout:
			logging.Error("timed out")
			ticker.Stop()
		}
		break
	}

	if !podHealthy {
		return "", fmt.Errorf("busybox pod has not become healthy after 60 seconds")
	}

	command := []string{"nsenter", "--target", "1", "--mount", "--uts", "--ipc", "--net", "--pid", "--", "sh", "-c", cmd}

	output, err := config.Kube.Pods().ExecNamed(pod.Namespace, pod.Name, command)
	if err != nil {
		return "", err
	}

	if len(output) != 0 {
		output += "\n" + output
	}

	return output, nil
}
