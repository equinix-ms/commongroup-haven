// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"encoding/json"
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
	"io/ioutil"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8syaml "sigs.k8s.io/yaml"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func externalCNCF(config *Config) (Result, error) {
	logging.Info("Check CNCF: Running compliancy external CNCF checker. This usually takes about 1.5 hours. See: https://github.com/vmware-tanzu/sonobuoy.\n")

	flag := ""

	if config.HostPlatform == PlatformOpenShift {
		flag += " --dns-namespace=openshift-dns --dns-pod-labels=dns.operator.openshift.io/daemonset-dns=default"
	}

	err := config.Sono.Run(flag)
	if err != nil {
		return ResultNo, err
	}

	// Intercept.
	ctrlc := false

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)

	progressBar, err := outPutProgress(config.Sono, config.GlobalInterval)
	if err != nil {
		return ResultNo, err
	}

	go func() {
		<-sigs

		logging.Warning("\n\n** User break: aborting **\n\n")
		progressBar.Finish()
		ctrlc = true
	}()

	progressBar.Start()

	for {
		if ctrlc {
			break
		}

		_, err = config.Sono.Retrieve()

		if err == nil {
			break
		}

		err = updateProgress(progressBar, config.Sono)
		if err != nil {
			continue
		}

		time.Sleep(config.GlobalInterval)
	}

	results := "Status: failed"

	// Process.
	if !ctrlc {
		progressBar.Finish()
		results, err = config.Sono.Results()
		if err != nil {
			return ResultNo, err
		}
	}

	// Cleanup.
	logging.Info("Check CNCF: Cleaning up sonobuoy deployment.\n")
	err = config.Sono.DeleteAll()
	if err != nil {
		return ResultNo, err
	}

	if !strings.Contains(results, "Status: failed") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

func updateProgress(pb *ProgressBar, sono sonobuoy.Client) error {
	status, err := sono.Status()
	if err != nil {
		return err
	}

	var e2ePlugin sonobuoy.Plugin
	for _, plugin := range status.Plugins {
		if plugin.Plugin == "e2e" {
			e2ePlugin = plugin
		}
	}

	if e2ePlugin.Progress == nil {
		return nil
	}

	pb.completed = e2ePlugin.Progress.Completed

	return nil
}

// outPutProgress shows the current progress of sonobuoy
func outPutProgress(sono sonobuoy.Client, interval time.Duration) (*ProgressBar, error) {
	var e2ePlugin sonobuoy.Plugin

	for {
		sonoStatus, err := sono.Status()
		if err != nil {
			//it takes a few moments to start up sonobuoy
			time.Sleep(interval)
			continue
		} else {
			for _, plugin := range sonoStatus.Plugins {
				if plugin.Plugin == "e2e" {
					e2ePlugin = plugin
					break
				}
			}

			if e2ePlugin.Progress != nil {
				break
			} else {
				time.Sleep(interval)
				continue
			}
		}
	}

	pb := NewProgressBar(e2ePlugin.Progress.Total, e2ePlugin.Progress.Completed)

	return pb, nil
}

// suggestionExternalCIS is a 'Suggested Check' meaning it's not counted towards Haven Compliancy.
// Automating the manual CIS Benchmark is done via kube-bench. There are many ways to run kube-bench.
// - Most importantly it should work on any Haven cluster.
// - Tested successfully on Kops/OpenStack, GKE, AKS and EKS.
func suggestionExternalCIS(config *Config) (Result, error) {
	logging.Info("Check CIS: Running suggested external CIS checker on the worker nodes. This should not take long. See: https://github.com/aquasecurity/kube-bench.\n")

	var job *batchv1.Job
	var platform string

	switch config.HostPlatform {
	case PlatformGKE:
		platform = GkeKey
	case PlatformEKS:
		platform = EksKey
	case PlatformAKS:
		platform = AksKey
	default:
		job = &batchv1.Job{
			ObjectMeta: metav1.ObjectMeta{
				Name: config.CisCheckAppName,
			},
			Spec: batchv1.JobSpec{
				Template: apiv1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app": config.CisCheckAppName,
						},
					},
					Spec: apiv1.PodSpec{
						HostPID:       true,
						RestartPolicy: apiv1.RestartPolicyNever,
						Containers: []apiv1.Container{
							{
								Name:    config.CisCheckAppName,
								Image:   "aquasec/kube-bench:latest",
								Command: []string{"kube-bench", "node", "--version", fmt.Sprintf("%d.%d", config.KubeServer.Major(), config.KubeServer.Minor())},
								VolumeMounts: []apiv1.VolumeMount{
									{
										Name:      "var-lib-kubelet",
										MountPath: "/var/lib/kubelet",
										ReadOnly:  true,
									},
									{
										Name:      "etc-systemd",
										MountPath: "/etc/systemd",
										ReadOnly:  true,
									},
									{
										Name:      "etc-kubernetes",
										MountPath: "/etc-kubernetes",
										ReadOnly:  true,
									},
								},
							},
						},
						Volumes: []apiv1.Volume{
							{
								Name: "var-lib-kubelet",
								VolumeSource: apiv1.VolumeSource{
									HostPath: &apiv1.HostPathVolumeSource{
										Path: "/var/lib/kubelet",
									},
								},
							},
							{
								Name: "etc-systemd",
								VolumeSource: apiv1.VolumeSource{
									HostPath: &apiv1.HostPathVolumeSource{
										Path: "/etc/systemd",
									},
								},
							},
							{
								Name: "etc-kubernetes",
								VolumeSource: apiv1.VolumeSource{
									HostPath: &apiv1.HostPathVolumeSource{
										Path: "/etc/kubernetes",
									},
								},
							},
						},
					},
				},
			},
		}
	}

	if config.HostPlatform == PlatformGKE || config.HostPlatform == PlatformAKS || config.HostPlatform == PlatformEKS {
		err := getKubeBench(platform, &job)
		if err != nil {
			return ResultNo, err
		}

		job.Name = config.CisCheckAppName
		for _, container := range job.Spec.Template.Spec.Containers {
			container.Name = config.CisCheckAppName
		}
	}

	if _, err := config.Kube.Jobs().Create(config.Namespace, job); err != nil {
		return ResultNo, err
	}

	defer func() {
		if err := config.Kube.Jobs().Delete(config.Namespace, config.CisCheckAppName, nil); err != nil {
			logging.Error("Delete Job '%s': %s", config.CisCheckAppName, err)
		}
	}()

	// Check Pod status max 1 minute.
	start := time.Now()
	ticker := time.NewTicker(config.GlobalInterval)
	defer ticker.Stop()

	for ts := range ticker.C {
		if ts.Sub(start).Seconds() >= 60 {
			return ResultNo, nil
		}

		label := fmt.Sprintf("job-name=%s", config.CisCheckAppName)
		pods, err := config.Kube.Pods().GetByLabel(config.Namespace, label)
		if err != nil {
			return ResultNo, err
		}

		if len(pods.Items) != 1 {
			continue
		}

		// When ready check the results in Pod logs.
		if pods.Items[0].Status.Phase == "Succeeded" {
			output, err := config.Kube.Pods().Logs(config.Namespace, pods.Items[0].ObjectMeta.Name)
			if err != nil {
				return ResultNo, err
			}

			logging.Output(output, "CIS")

			if strings.Contains(output, "0 checks FAIL") {
				return ResultYes, nil
			}

			break
		}
	}

	return ResultNo, nil
}

// getKubeBench sets the static yaml to a kubernetes usable object
func getKubeBench(platform string, v interface{}) error {
	name := fmt.Sprintf("static/kube-bench/job-%s.yaml", platform)

	f, err := embedStatic.Open(name)
	if err != nil {
		return err
	}

	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}

	g, err := k8syaml.YAMLToJSON(b)
	if err != nil {
		return err
	}

	err = json.Unmarshal(g, &v)
	if err != nil {
		return err
	}

	return nil
}
