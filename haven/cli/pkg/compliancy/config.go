// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package compliancy

import (
	"github.com/Masterminds/semver/v3"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/haven"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
	"time"
)

// Config holds the configuration of the checker
type Config struct {
	KubeServer      *semver.Version
	Kube            kubernetes.KubeClient
	Sono            sonobuoy.Client
	CrdClient       haven.V1Alpha1
	KubeLatest      string
	Namespace       string
	CNCFConfigured  bool
	RunCISChecks    bool
	HostPlatform    Platform
	CisCheckAppName string
	GlobalInterval  time.Duration
}
