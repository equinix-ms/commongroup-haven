package sonobuoy

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestSonoMock(t *testing.T) {
	t.Run("RunPass", func(t *testing.T) {
		sonoTestClient := NewMockSonobuoyClient(2, 100, 0, "", "")
		err := sonoTestClient.Run("")
		assert.Nil(t, err)
	})

	t.Run("RunFail", func(t *testing.T) {
		expected := "mockflag"
		sonoTestClient := NewMockSonobuoyClient(2, 100, 0, "", "Run")
		err := sonoTestClient.Run(expected)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), expected)
		assert.Contains(t, err.Error(), DefaultError)
	})

	t.Run("StatusPass", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "running"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "")
		sut, err := sonoTestClient.Status()
		assert.Nil(t, err)
		assert.Equal(t, total, sut.Plugins[0].Progress.Total)
		assert.Equal(t, completed, sut.Plugins[0].Progress.Completed)
		assert.Equal(t, expected, sut.Plugins[0].Status)
	})

	t.Run("StatusCompleted", func(t *testing.T) {
		total := int64(100)
		completed := int64(100)
		expected := "completed"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "")
		sut, err := sonoTestClient.Status()
		assert.Nil(t, err)
		assert.Equal(t, total, sut.Plugins[0].Progress.Total)
		assert.Equal(t, completed, sut.Plugins[0].Progress.Completed)
		assert.Equal(t, expected, sut.Plugins[0].Status)
	})

	t.Run("StatusFail", func(t *testing.T) {
		sonoTestClient := NewMockSonobuoyClient(2, 100, 0, "", "Status")
		_, err := sonoTestClient.Status()
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), DefaultError)
	})

	t.Run("StatusPassAfterTwoRuns", func(t *testing.T) {
		total := int64(100)
		completed := int64(100)
		expected := "completed"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 1, "", "")
		_, err := sonoTestClient.Status()
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), DefaultError)
		sut, err := sonoTestClient.Status()
		assert.Nil(t, err)
		assert.Equal(t, total, sut.Plugins[0].Progress.Total)
		assert.Equal(t, completed, sut.Plugins[0].Progress.Completed)
		assert.Equal(t, expected, sut.Plugins[0].Status)
	})

	t.Run("RetrievePassesAfterTwoCalls", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "tar.gz"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "")
		sut, err := sonoTestClient.Retrieve()
		assert.NotNil(t, err)
		assert.Equal(t, "", sut)
		sut, err = sonoTestClient.Retrieve()
		assert.Nil(t, err)
		assert.Contains(t, sut, expected)
	})

	t.Run("RetrieveFail", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "no valid entries in result"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "Retrieve")
		sut, err := sonoTestClient.Retrieve()
		assert.NotNil(t, err)
		assert.Equal(t, "", sut)
		assert.Contains(t, err.Error(), expected)
		assert.Contains(t, err.Error(), DefaultError)
	})

	t.Run("ResultsPass", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "Passed"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "")
		sut, err := sonoTestClient.Results()
		assert.Nil(t, err)
		assert.Contains(t, sut, expected)
		assert.Contains(t, sut, strings.ToLower(expected))
	})

	t.Run("ResultsPassWithFailedTests", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "failed"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "failed", "")
		sut, err := sonoTestClient.Results()
		assert.Nil(t, err)
		assert.Contains(t, sut, expected)
	})

	t.Run("ResultsFail", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "results"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "Results")
		sut, err := sonoTestClient.Results()
		assert.NotNil(t, err)
		assert.Equal(t, "", sut)
		assert.Contains(t, err.Error(), expected)
		assert.Contains(t, err.Error(), DefaultError)
	})

	t.Run("DeletePassed", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "")
		err := sonoTestClient.DeleteAll()
		assert.Nil(t, err)
	})

	t.Run("DeleteFailed", func(t *testing.T) {
		total := int64(100)
		completed := int64(2)
		expected := "delete"
		sonoTestClient := NewMockSonobuoyClient(completed, total, 0, "", "Delete")
		err := sonoTestClient.DeleteAll()
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), expected)
		assert.Contains(t, err.Error(), DefaultError)
	})
}
