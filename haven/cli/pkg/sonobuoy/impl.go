package sonobuoy

import (
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"os/exec"
	"strings"
)

// Client interface creates a client to interact with sono which is also mockable
type Client interface {
	Run(flags string) error
	Status() (*SonobuoyStatus, error)
	Retrieve() (string, error)
	Results() (string, error)
	DeleteAll() error
}

const DefaultMode string = "certified-conformance"

type Impl struct {
	Mode       string
	OutputFile string
}

// NewSonobuoyClient creates a new client interface
func NewSonobuoyClient(mode string) Client {
	if mode == "" {
		mode = DefaultMode
	}

	impl := Impl{
		Mode:       mode,
		OutputFile: "",
	}

	return &impl
}

func (s *Impl) Run(flags string) error {
	run := fmt.Sprintf("sonobuoy run --mode=%s", s.Mode)

	if flags != "" {
		run += flags
	}

	// Run.
	cmd := strings.Fields(run)
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()

	if err != nil {
		if !strings.Contains(string(out), "namespace already exists") {
			return fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
		}

		logging.Info("Check CNCF: Sonobuoy namespace already exists: resuming check.\n")
	}

	return nil
}

func (s *Impl) Status() (*SonobuoyStatus, error) {
	run := "sonobuoy status --json"
	cmd := strings.Fields(run)
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()
	if err != nil {
		return nil, err
	}

	sonoBuoy, err := UnmarshalSonobuoyStatus(out)
	if err != nil {
		return nil, err
	}

	return &sonoBuoy, nil
}

func (s *Impl) Retrieve() (string, error) {
	cmd := strings.Fields("sonobuoy retrieve")
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()
	if err != nil {
		return "", err
	}

	s.OutputFile = string(out)
	return string(out), err
}

func (s *Impl) Results() (string, error) {
	if s.OutputFile == "" {
		_, err := s.Retrieve()
		if err != nil {
			return "", nil
		}
	}
	cmd := strings.Fields(fmt.Sprintf("sonobuoy results %s", s.OutputFile))
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()

	return string(out), err
}

func (s *Impl) DeleteAll() error {
	cmd := strings.Fields("sonobuoy delete --all")
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()
	if err != nil {
		return fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
	}
	return nil
}
