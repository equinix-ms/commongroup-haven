package validator

import "regexp"

func ValidOrganization(orgName string) bool {
	regex := "(^[A-Za-z0-9-]*$)"
	re := regexp.MustCompile(regex)
	match := re.MatchString(orgName)

	return match
}
