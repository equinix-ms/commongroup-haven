// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package addons

import (
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdUpgrade(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to upgrade")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		//we have a separate flow for nlx since that requires multiple helm installs
		if *name == "nlx" {
			UpgradeNLXInstall()
			return
		}

		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kubeImpl, err := kubernetes.New(kubePath)
		if err != nil {
			logging.Error("Error creating kube client %s", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace, kubeImpl)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		logging.Info("Upgrading %s\n", addon.Name)

		_, err = helmClient.Upgrade(
			addon.Name,
			fmt.Sprintf("%s/%s", addon.Chart.Repository.Name, addon.Chart.Name),
			nil,
		)

		if err != nil {
			logging.Error("Error upgrading release: %s\n", err)
			return
		}

		logging.Info("Upgraded addon %s in namespace %s", addon.Name, addon.Namespace)
	}
}
