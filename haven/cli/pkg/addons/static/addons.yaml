addons:
  - name: traefik
    namespace: traefik
    description: Traefik Ingress Controller
    chart:
      name: traefik
      repository:
        name: traefik
        url: https://containous.github.io/traefik-helm-chart
    defaultValuesFile: static/traefik/traefik-values.yaml
  - name: ingress-nginx
    namespace: ingress-nginx
    description: NGINX Ingress Controller
    chart:
      name: ingress-nginx
      repository:
        name: ingress-nginx
        url: https://kubernetes.github.io/ingress-nginx
    defaultValuesFile: static/ingress-nginx/ingress-nginx-values.yaml
  - name: cert-manager
    namespace: cert-manager
    description: Automatically provision and manage TLS certificates
    questions:
      - name: "%CLUSTERISSUER_EMAIL%"
        message: Please enter a valid email address used to register with LetsEncrypt
    chart:
      name: cert-manager
      repository:
        name: jetstack
        url: https://charts.jetstack.io
    defaultValuesFile: static/cert-manager/cert-manager-values.yaml
    postInstallYaml:
      - name: clusterissuer
        location: static/cert-manager/clusterissuer.yaml
  - name: prometheus-stack
    namespace: monitoring
    description: Monitoring system and time series database
    questions:
      - name: "%MONITORING_HOST%"
        message: What will be the hostname of the dashboard?
        help: "Use the fully qualified domain name (FQDN). For example: monitoring.haven.commonground.nl"
      - name: "%ADMIN_PASSWORD%"
        message: What will be the password of the admin?
        default:
          randString: 14
    chart:
      name: kube-prometheus-stack
      repository:
        name: prometheus-community
        url: https://prometheus-community.github.io/helm-charts
    defaultValuesFile: static/prometheus-stack/prometheus-stack-values.yaml
  - name: loki
    namespace: monitoring
    description: Multi-tenant log aggregation system
    chart:
      name: loki-stack
      repository:
        name: loki
        url: https://grafana.github.io/loki/charts
    defaultValuesFile: static/loki/loki-values.yaml
  - name: postgres-operator
    namespace: kube-system
    description: Zalando Postgres operator
    chart:
      name: postgres-operator
      repository:
        name: commonground
        url: https://charts.commonground.nl/
    defaultValuesFile: static/postgres-operator/postgres-operator-values.yaml
  - name: nlx
    namespace: nlx
    description: Offering an API via an NLX inway
    questions:
      - name: "INWAY"
        message: Do you want to install inway?
        default:
          string: "yes"
      - name: "OUTWAY"
        message: Do you want to install outway?
        default:
          string: "yes"
      - name: "INSTALLPOSTGRES"
        message: Do you want to install postgres?
        default:
          string: "yes"
      - name: "CERTDIR"
        message: Directory to use for certificates files
        help: "This will default to your temp directory - which will be deleted at restart"
        default:
          string: "/tmp"
      - name: "%CLUSTERISSUER_EMAIL%"
        message: Please enter a valid email address used to register with LetsEncrypt
        default:
          string: "example@test.com"
      - name: "%INWAYURL%"
        message: Host name where your NLX inway can be reached (needed for the external certificate)
        help: "Use the fully qualified domain name (FQDN). For example: inway.haven.commonground.nl"
        default:
          string: "inway.dev.haven.vng.cloud"
      - name: "ORGNAME"
        message: Url friendly name with a maximum length of 100 characters (needed for the external certificate)
        help: "Use a name that can be part of a url (no dots allowed0. For example: mijn-organisatie"
        default:
          string: "management-haven"
      - name: "%MANAGEMENTURL%"
        message: Host name where your NLX management can be reached (needed for the external certificate)
        help: "Use the fully qualified domain name (FQDN). For example: management.haven.commonground.nl"
        default:
          string: "management.dev.haven.vng.cloud"
      - name: "%ADMINUSER%"
        message: What will be the admin user for NLX Management
        default:
          string: "admin@notanemail.com"
      - name: "%ADMINPASSWORD%"
        message: What will be the password of the admin for NLX Management?
        default:
          randString: 14
    charts:
      - name: nlx-management
        namespace: nlx
        mainChart: true
        repository:
          name: commonground
          url: https://charts.commonground.nl
        defaultValuesFile: static/nlx/management/nlx-management-values.yaml
        preInstallYaml:
          - name: management-internal-tls
            location: static/nlx/management/management-internal-tls.yaml
        postInstallYaml:
          - name: management-api-create-user
            location: static/nlx/management/job-create-administrator.yaml
      - name: nlx-inway
        namespace: nlx
        repository:
          name: commonground
          url: https://charts.commonground.nl
        defaultValuesFile: static/nlx/inway/nlx-inway-values.yaml
        preInstallYaml:
          - name: inway-internal-tls
            location: static/nlx/inway/inway-internal-tls.yaml
      - name: nlx-outway
        namespace: nlx
        repository:
          name: commonground
          url: https://charts.commonground.nl
        defaultValuesFile: static/nlx/outway/nlx-outway-values.yaml
        preInstallYaml:
          - name: outway-internal-tls
            location: static/nlx/outway/outway-internal-tls.yaml
      - name: postgresql
        namespace: nlx
        repository:
          name: bitnami
          url: https://charts.bitnami.com/bitnami
          version: 10.6.0
        defaultValuesFile: static/nlx/bitnami/postgres-values.yaml
      - name: cert-manager
        namespace: cert-manager
        description: Automatically provision and manage TLS certificates
        repository:
          name: jetstack
          url: https://charts.jetstack.io
        defaultValuesFile: static/cert-manager/cert-manager-values.yaml
        postInstallYaml:
          - name: internal
            location: static/cert-manager/clusterissuer.yaml
          - name: internalissuer
            location: static/nlx/cert/internal-issuer.yaml
