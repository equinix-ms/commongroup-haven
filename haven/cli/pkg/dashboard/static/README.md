# Haven dashboard

This folder should contain a build of the Haven dashboard and will be included in the Haven CLI binary. To manually build the dashboard use:

```bash
(cd community/addons/dashboard && npm install && npm run build)
(cp -R community/addons/dashboard/build/* haven/cli/pkg/dashboard/static/)
```

Currently, the haven dashboard is dependent on [flux](https://fluxcd.io/) to get most of its data. This will be fixed in the future
please refer to this [issue](https://gitlab.com/commonground/haven/haven/-/issues/487) to see progress.
