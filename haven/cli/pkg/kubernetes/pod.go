package kubernetes

import (
	"bytes"
	"context"
	"io"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/remotecommand"
	"time"
)

type PodImpl struct {
	client v1.CoreV1Interface
	config []byte
}

// NewPodsClient to interact with Pod interface
func NewPodsClient(kube kubernetes.Interface, config []byte) (*PodImpl, error) {
	coreClient := kube.CoreV1()

	return &PodImpl{client: coreClient, config: config}, nil
}

// List lists all pods within your cluster
func (p *PodImpl) List(namespace string) (*corev1.PodList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pods, err := p.client.Pods(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return pods, nil
}

// Get a named pod from a namespace in your cluster
func (p *PodImpl) Get(namespace, podName string) (*corev1.Pod, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	namedPod, err := p.client.Pods(namespace).Get(ctx, podName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return namedPod, nil
}

// GetByLabel gets a pod from a namespace in your cluster based on the label
func (p *PodImpl) GetByLabel(namespace, label string) (*corev1.PodList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pods, err := p.client.Pods(namespace).List(ctx, metav1.ListOptions{LabelSelector: label})
	if err != nil {
		return nil, err
	}

	return pods, nil
}

// Delete a pod in a namespace in your cluster
func (p *PodImpl) Delete(namespace, podName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := p.client.Pods(namespace).Delete(ctx, podName, metav1.DeleteOptions{})

	return err
}

// Create a pod in a namespace in your cluster
func (p *PodImpl) Create(namespace string, pod *corev1.Pod) (*corev1.Pod, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pod, err := p.client.Pods(namespace).Create(ctx, pod, metav1.CreateOptions{})

	return pod, err
}

// Logs gets the logs from a pod in the cluster
func (p *PodImpl) Logs(namespace, name string) (string, error) {
	logs := p.client.Pods(namespace).GetLogs(name, &corev1.PodLogOptions{})

	stream, err := logs.Stream(context.Background())
	if err != nil {
		return "", err
	}

	buf := new(bytes.Buffer)
	_, err = io.Copy(buf, stream)

	if err := stream.Close(); err != nil {
		return "", err
	}

	if err != nil {
		return "", err
	}

	output := buf.String()

	return output, nil
}

func (p *PodImpl) ExecNamed(namespace, podName string, command []string) (string, error) {
	kubeCfg := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		clientcmd.NewDefaultClientConfigLoadingRules(),
		&clientcmd.ConfigOverrides{},
	)

	p.getConfig()

	restCfg, err := kubeCfg.ClientConfig()
	if err != nil {
		return "", err
	}

	commandBuffer := &bytes.Buffer{}
	errBuffer := &bytes.Buffer{}

	req := p.client.RESTClient().Post().
		Resource("pods").Name(podName).Namespace(namespace).SubResource("exec").
		VersionedParams(&corev1.PodExecOptions{
			Command: command,
			Stdin:   false,
			Stdout:  true,
			Stderr:  true,
			TTY:     true,
		}, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(restCfg, "POST", req.URL())
	if err != nil {
		return errBuffer.String(), err
	}

	err = exec.Stream(remotecommand.StreamOptions{
		Stdout: commandBuffer,
		Stderr: errBuffer,
	})

	if err != nil {
		return errBuffer.String(), err
	}

	return commandBuffer.String(), nil
}

func (p *PodImpl) getConfig() []byte {
	return p.config
}
