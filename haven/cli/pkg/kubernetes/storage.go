package kubernetes

import (
	"context"
	"k8s.io/api/storage/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	storv1 "k8s.io/client-go/kubernetes/typed/storage/v1"
	"time"
)

type StorageImpl struct {
	client storv1.StorageV1Interface
}

// NewStorageClient to interact with Storage interface
func NewStorageClient(kube kubernetes.Interface) (*StorageImpl, error) {
	coreClient := kube.StorageV1()

	return &StorageImpl{client: coreClient}, nil
}

// List all storageclasses in your kube cluster
func (s *StorageImpl) List() (*v1.StorageClassList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	sc, err := s.client.StorageClasses().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	return sc, nil
}
