FROM golang:1.16.5-alpine AS build

WORKDIR /go/src/haven

COPY go.mod go.sum ./
COPY cmd cmd
COPY pkg pkg

ARG VERSION=unknown
ARG KUBE_LATEST=unknown

RUN addgroup -S -g 1001 haven && adduser -S -D -H -G haven -u 1001 haven
RUN CGO_ENABLED=0 \
    go build -v \
      -ldflags "-w -s -X 'main.version=${VERSION}' -X 'gitlab.com/commonground/haven/haven/haven/cli/pkg/compliancy.kubeLatest=${KUBE_LATEST}'" \
      -o /go/bin/haven \
      ./cmd/cli

FROM alpine:3.14.0

COPY --from=build /go/bin/haven /usr/local/bin/haven
COPY --from=build /etc/passwd /etc/group /etc/

USER haven
ENTRYPOINT ["/usr/local/bin/haven"]
