---
icon: euro
---

### Eenvoudig beheer

Systeembeheerders kunnen verschillende applicaties op dezelfde manier beheren, in plaats van allerlei verschillende installatieprocedures per applicatie. Hierdoor wordt het een stuk eenvoudiger om veel applicaties tegelijkertijd te beheren.

Dit reduceert kosten, evenals de voordelen van cloud zoals het automatisch op- en afschalen van benodigde rekenkracht.
