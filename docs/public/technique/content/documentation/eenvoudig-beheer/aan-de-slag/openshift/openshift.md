---
title: "Red Hat OpenShift"
path: "/aan-de-slag/openshift"
---

Haven op basis van Red Hat OpenShift is installeerbaar op verschillende onderliggende cloud en on-premise platformen. Er zijn hiertoe meerdere Referentie Implementaties beschikbaar.

- [Azure Red Hat OpenShift](https://docs.microsoft.com/nl-nl/azure/openshift/) (managed) - [_Aan de slag_](/techniek/aan-de-slag/aro)

- [Red Hat OpenShift op Azure](https://cloud.redhat.com/openshift/install/azure/installer-provisioned) - [_Aan de slag_](/techniek/aan-de-slag/openshift-op-azure)

- [Red Hat OpenShift op AWS](https://cloud.redhat.com/openshift/install/aws/installer-provisioned) - [_Aan de slag_](/techniek/aan-de-slag/openshift-op-aws)

- [Red Hat OpenShift op GCP](https://cloud.redhat.com/openshift/install/gcp/installer-provisioned) - [_Aan de slag_](/techniek/aan-de-slag/openshift-op-gcp)

- [Red Hat OpenShift op On-premise](https://www.redhat.com/en/technologies/cloud-computing/openshift)
