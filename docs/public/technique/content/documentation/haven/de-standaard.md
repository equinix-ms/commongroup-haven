---
title: Hoe het werkt
path: "/"
---

Haven is een standaard voor platform-onafhankelijke cloud hosting. Haven schrijft een specifieke configuratie van [Kubernetes](https://www.kubernetes.io) voor die dient te worden geïmplementeerd op bestaande technische infrastructuur, bijvoorbeeld een cloud of on-premise platform.

De [voorgeschreven configuratie](/techniek/checks) zorgt ervoor dat iedere Haven omgeving functioneel gelijk is ongeacht de onderliggende technische infrastructuur. Zie het als een abstractielaag die resulteert in een gezamenlijk vertrekpunt.

Dit brengt diverse voordelen met zich mee: uniformiteit in technische infrastructuur, uitwisselbaarheid van toepassingen, leveranciersonafhankelijkheid, platformonafhankelijkheid en kostenreductie.

Implementatie van een Haven omgeving kan door gemeenten zelf worden gedaan of door een leverancier naar keuze. Diverse [Referentie Implementaties](/techniek/aan-de-slag) kunnen hierbij ondersteunen.

## Haven Compliancy Checker

_De [Haven Compliancy Checker](/techniek/compliancy-checker) borgt de standaard door geautomatiseerd clusters te valideren. Dit is de essentie van Haven en de validatie staat op zichzelf._

Gebruik van een [Referentie Implementatie](https://gitlab.com/commonground/haven/haven/-/tree/master/reference) of [Addons](/techniek/addons) is dus niet verplicht en heeft geen invloed op Haven Compliancy.

Er zijn verschillende implementaties van leveranciers en cloudproviders waarvan we hebben vastgesteld dat ze Haven Compliant zijn. Bekijk voor meer informatie de [aan de slag](/techniek/aan-de-slag) pagina.

## Beheer van de standaard

De Haven standaard is zoals iedere standaard onderhevig aan wijzigingen. Ontwikkelingen van (cloud) providers, leveranciers en gebruikte technologie volgen elkaar immers in hoog tempo op. Uitgangspunt is balans te houden tussen nut en noodzaak van updates en eventuele druk op beheer.

Een voorstel tot het wijzigen, toevoegen of verwijderen van een check in de Haven Compliancy Checker - en daarmee de Haven Standaard - kan als volgt worden ingediend:

- Open een Haven [Gitlab issue](https://gitlab.com/commonground/haven/haven/-/issues/new?issue) met label "Standard Change". Deze issue is publiek inzichtelijk.
- De community is vervolgens uitgenodigd om te reageren op het voorstel in de issue.
- Het Haven kern team besluit op basis van de input van de community of het voorstel geaccepteerd of geweigerd wordt.
- Volgens het Haven release management zullen eventuele wijzigingen vervolgens worden meegenomen naar nieuwe versies van Haven.

Een voorbeeld: Haven vereist dat een Kubernetes cluster wordt voorzien van een logging voorziening. Hiertoe is een _whitelist_ opgesteld van bekende toepassingen die hierin kunnen voorzien. Indien er een nieuwe toepassing in beeld komt die ook in logging kan voorzien, kan deze worden opgenomen in de whitelist van de bewuste check.

## Release management

### Versionering

Haven werkt met Major.Minor.Patch nummerieke versionering. Een voorbeeld: v10.1.3.

Wijzigingen aan de standaard die mogelijk impact hebben op beheer brengen automatisch een ophoging van de Major versie met zich mee. Een voorbeeld: een nieuw te installeren onderdeel in het cluster is vereist.

Wijzigingen die geen impact hebben op beheer zullen middels Minor en Patch versies worden uitgegeven. Denk aan het uitbreiden van een whitelist, het toevoegen van een Referentie Implementatie of het aanpassen van documentatie.

### Releases

Haven streeft er naar om nieuwe Major releases per kwartaal uit te brengen, dat wil zeggen januari, april, juli en oktober. Minor en Patch versies kunnen doorlopend worden uitgegeven.

Om met een bestaande Haven omgeving aan Haven Compliancy en daarmee aan de standaard te blijven voldoen, is het noodzakelijk om minimaal ieder kwartaal de Haven Compliancy Checker opnieuw met succes uit te voeren.

Wanneer een omgeving reeds Haven Compliant is en een nieuwe Major versie van Haven wordt uitgebracht, kan het voorkomen dat de Haven Compliancy Checker aangeeft dat de bestaande omgeving niet meer Haven Compliant is.

In voorkomend geval is er een tijdsvak van 3 maanden om de nodige wijzigingen door te voeren aan de bestaande omgeving en deze opnieuw Haven Compliant te maken.
