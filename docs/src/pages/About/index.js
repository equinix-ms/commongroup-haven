// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import Head from 'next/head'
import Header from 'src/components/Header'
import News from 'src/components/NewsSection'
import Footer from 'src/components/Footer'
import Introduction from './Introduction'
import Background from './Background'
import Commonground from './Commonground'

const About = ({ sections }) => (
  <div>
    <Head>
      <title>Haven - Over</title>
      <meta name="description" content={sections.meta.description} />
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <Background {...sections.background} />
      <Commonground {...sections.commonground} />
      <News {...sections.news} />
    </main>

    <Footer />
  </div>
)

About.propTypes = {
  sections: object.isRequired,
}

export default About
