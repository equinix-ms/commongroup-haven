// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Media } from 'src/styling/media'
import DiagramDesktop from './DiagramDesktop'
import DiagramMobile from './DiagramMobile'
import { Wrapper } from './index.styles'

const Diagram = ({ diagramA11yText, diagramFeaturesLinkHref }) => {
  return (
    <Wrapper>
      <Media lessThan="lg">
        <DiagramMobile
          diagramA11yText={diagramA11yText}
          diagramFeaturesLinkHref={diagramFeaturesLinkHref}
        />
      </Media>

      <Media greaterThanOrEqual="lg">
        <DiagramDesktop
          diagramA11yText={diagramA11yText}
          diagramFeaturesLinkHref={diagramFeaturesLinkHref}
        />
      </Media>
    </Wrapper>
  )
}

Diagram.propTypes = {
  diagramA11yText: string,
  diagramFeaturesLinkHref: string,
}

export default Diagram
