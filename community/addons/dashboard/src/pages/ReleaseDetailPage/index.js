// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { string } from 'prop-types'
import { Link, useParams, useHistory, useLocation } from 'react-router-dom'
import { Alert, Drawer } from '@commonground/design-system'
import { useTranslation } from 'react-i18next'
import usePromise from '../../hooks/use-promise'
import LoadingMessage from '../../components/LoadingMessage'
import EditButton from '../../components/EditButton'
import services from '../../services'
import {
  StyledActionsBar,
  StyledRemoveButton,
  StyledSpecList,
} from './index.styles'

const ReleaseDetailPage = ({ parentUrl }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const location = useLocation()

  const { namespace, name } = useParams()

  const [isRemoved, setIsRemoved] = useState(false)
  const { isReady, error, result } = usePromise(
    services.helmReleases.get,
    namespace,
    name,
  )

  const handleRemove = () => {
    if (window.confirm(t('Do you want to remove the release?'))) {
      services.helmReleases.delete(namespace, name)
      setIsRemoved(true)
    }
  }

  return (
    <Drawer noMask closeHandler={() => history.push(parentUrl)}>
      <Drawer.Header title={name} closeButtonLabel={t('Close')} />
      <Drawer.Content>
        {!isReady || (!error && !result) ? (
          <LoadingMessage />
        ) : error ? (
          <Alert variant="error">
            {t('Failed to load the release.', { name })}
          </Alert>
        ) : isRemoved ? (
          <Alert variant="success">{t('The release has been removed.')}</Alert>
        ) : result ? (
          <>
            <StyledActionsBar>
              <EditButton as={Link} to={`${location.pathname}/edit`} />
              <StyledRemoveButton onClick={handleRemove} />
            </StyledActionsBar>

            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.metadata.name}
              />
              <StyledSpecList.Item
                title={t('Namespace')}
                value={result.metadata.namespace}
              />
            </StyledSpecList>

            <h3>{t('Chart')}</h3>
            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Name')}
                value={result.spec.chart.spec.chart}
              />
              <StyledSpecList.Item
                title={t('Version')}
                value={result.spec.chart.spec.version}
              />
            </StyledSpecList>

            <h3>{t('Source')}</h3>
            <StyledSpecList alignValuesRight>
              <StyledSpecList.Item
                title={t('Kind')}
                value={result.spec.chart.spec.sourceRef.kind}
              />
              <StyledSpecList.Item
                title={t('Name')}
                value={result.spec.chart.spec.sourceRef.name}
              />
              <StyledSpecList.Item
                title={t('Namespace')}
                value={result.spec.chart.spec.sourceRef.namespace}
              />
            </StyledSpecList>

            <h3>{t('Status')}</h3>

            <StyledSpecList alignValuesRight>
              {result.status.conditions.map((condition, i) => (
                <StyledSpecList.Item
                  key={i}
                  title={condition.lastTransitionTime}
                  value={`${condition.message} (${condition.reason})`}
                />
              ))}
            </StyledSpecList>
          </>
        ) : null}
      </Drawer.Content>
    </Drawer>
  )
}

ReleaseDetailPage.propTypes = {
  parentUrl: string,
}

export default ReleaseDetailPage
