// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//

import React, { useState } from 'react'
import { useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Alert } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import LoadingMessage from '../../components/LoadingMessage'
import RepositoryForm from '../../components/RepositoryForm'
import usePromise from '../../hooks/use-promise'
import services from '../../services'

const EditRepositoryPage = () => {
  const { t } = useTranslation()
  const { namespace, name } = useParams()
  const [isProcessing, setIsProcessing] = useState(false)
  const [isEdited, setIsEdited] = useState(false)
  const [submitError, setSubmitError] = useState(null)

  const { isReady, error, result } = usePromise(
    services.helmRepositories.get,
    namespace,
    name,
  )

  const submitRelease = async (release) => {
    setSubmitError(null)
    setIsProcessing(true)

    try {
      await services.helmRepositories.update(namespace, name, release)
      setIsEdited(true)
    } catch (e) {
      setSubmitError(e.message)
    }

    setIsProcessing(false)
  }

  return (
    <PageTemplate>
      <PageTemplate.HeaderWithBackNavigation
        backButtonTo="/repositories"
        title={t('Edit repository')}
      />
      {!isReady ? <LoadingMessage /> : null}

      {error || submitError ? (
        <Alert title={t('Failed editing repository')} variant="error">
          {error || submitError}
        </Alert>
      ) : null}

      {isEdited && !error ? (
        <Alert variant="success">{t('The repository has been edited.')}</Alert>
      ) : null}

      {!isEdited && result ? (
        <RepositoryForm
          submitButtonText={t('Edit')}
          initialValues={result}
          onSubmitHandler={submitRelease}
          disableForm={isProcessing}
        />
      ) : null}
    </PageTemplate>
  )
}

export default EditRepositoryPage
