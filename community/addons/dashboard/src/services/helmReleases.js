// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
export class HelmReleasesService {
  async list() {
    const result = await fetch(
      '/apis/helm.toolkit.fluxcd.io/v2beta1/helmreleases/',
    )
    if (!result.ok) {
      throw new Error('error occured while getting releases list')
    }

    return result.json()
  }

  async get(namespace, name) {
    const result = await fetch(
      `/apis/helm.toolkit.fluxcd.io/v2beta1/namespaces/${namespace}/helmreleases/${name}`,
    )
    if (!result.ok) {
      throw new Error('error occured while getting release')
    }

    return result.json()
  }

  async create(namespace, release) {
    const result = await fetch(
      `/apis/helm.toolkit.fluxcd.io/v2beta1/namespaces/${namespace}/helmreleases`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(release),
      },
    )

    if (!result.ok) {
      throw new Error('error while creating release')
    }

    return result.json()
  }

  async update(namespace, name, release) {
    const result = await fetch(
      `/apis/helm.toolkit.fluxcd.io/v2beta1/namespaces/${namespace}/helmreleases/${name}`,
      {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(release),
      },
    )

    if (!result.ok) {
      throw new Error('error occured while updating release')
    }

    return result.json()
  }

  async delete(namespace, name) {
    const result = await fetch(
      `/apis/helm.toolkit.fluxcd.io/v2beta1/namespaces/${namespace}/helmreleases/${name}`,
      { method: 'DELETE' },
    )
    if (!result.ok) {
      throw new Error('error occured while deleting release')
    }

    return result.json()
  }
}
