// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
import { HelmReleasesService } from './helmReleases'
import { HelmRepositoriesService } from './helmRepositories'
import { NamespacesService } from './namespaces'
import { CompliancyService } from './compliancies'

const services = {
  helmReleases: new HelmReleasesService(),
  helmRepositories: new HelmRepositoriesService(),
  namespaces: new NamespacesService(),
  compliancies: new CompliancyService(),
}

export default services
