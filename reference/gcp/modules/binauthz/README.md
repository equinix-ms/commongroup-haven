# Binary Authorization

Sets up binary authorization for the cluster.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| project_id | GCP project ID | <code title="">string</code> | ✓ |  |
| *admission_allowlist* | Allowed containers | <code title="list&#40;string&#41;">list(string)</code> |  | <code title="">["docker.io/nginxinc/*"]</code> |
| *attestor_name* | Attestor base name | <code title="">string</code> |  | <code title="">haven-compliance</code> |
| *compliance_testing_use* | Set to true if this cluster is being used for compliance testing | <code title="">bool</code> |  | <code title="">true</code> |
| *enforcement_mode* | Enforcement mode | <code title="">string</code> |  | <code title="">DRYRUN_AUDIT_LOG_ONLY</code> |

## Outputs

<!-- END TFDOC -->
