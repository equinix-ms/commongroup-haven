#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

output "cluster_name" {
  value = module.gke.name
  depends_on = [
    module.gke-nodepool
  ]
}

output "cluster_id" {
  value = module.gke.cluster.id
  depends_on = [
    module.gke-nodepool
  ]
}

output "cluster_region" {
  value = module.gke.location
}

output "cluster_network" {
  value = var.gke_network
}

output "kubernetes_endpoint" {
  description = "GKE control plane endpoint"
  sensitive   = true
  value       = format("https://%s", module.gke.endpoint)
  depends_on = [
    module.gke-nodepool
  ]
}

output "ca_certificate" {
  description = "GKE control plane CA certificate"
  value       = base64decode(module.gke.ca_certificate)
}

output "service_account" {
  description = "The default service account used for running nodes."
  value       = module.gke-nodepool.service_account
}

