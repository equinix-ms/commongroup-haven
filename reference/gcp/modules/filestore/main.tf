#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0.0"
    }
  }
}

data "google_compute_zones" "zones" {
  project = var.project_id
  region  = var.region
}

resource "google_filestore_instance" "instance" {
  project = var.project_id

  name = var.name
  zone = data.google_compute_zones.zones.names[0]
  tier = "BASIC_HDD"

  file_shares {
    capacity_gb = 1024
    name        = var.share_name
  }

  networks {
    network           = var.network
    modes             = ["MODE_IPV4"]
    reserved_ip_range = var.cidr_range
  }
}
