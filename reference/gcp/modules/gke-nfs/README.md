# NFS setup

Sets up NFS subdirectory provisioner in the cluster so workloads can share a NFS
mount.

<!-- BEGIN TFDOC -->
## Variables

| name | description | type | required | default |
|---|---|:---: |:---:|:---:|
| cluster_name | GKE cluster name | <code title="">string</code> | ✓ |  |
| nfs_host | NFS server hostname or IP | <code title="">string</code> | ✓ |  |
| nfs_share_path | NFS share path with leading path separator | <code title="">string</code> | ✓ |  |
| *namespace* | Namespace to install NFS provisioner in | <code title="">string</code> |  | <code title="">kube-system</code> |
| *path_pattern* | Path pattern for workloads | <code title="">string</code> |  | <code title="">$${.PVC.namespace}/$${.PVC.annotations.nfs.io/storage-path}</code> |

## Outputs

| name | description | sensitive |
|---|---|:---:|
| metadata | None |  |
<!-- END TFDOC -->
