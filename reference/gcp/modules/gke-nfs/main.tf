#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
  }
}

# Add NFS subdir provisioner
# 
# (To be changed to Filestore CSI driver, when Terraform support arrives,
# for more information, see: https://cloud.google.com/kubernetes-engine/docs/how-to/persistent-volumes/filestore-csi-driver)
# 
resource "helm_release" "nfs-provisioner" {
  name        = "nfs-subdir-external-provisioner"
  description = format("NFS subdirectory provisioner for: %s", var.cluster_name)
  namespace   = var.namespace

  repository = "https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner"
  chart      = "nfs-subdir-external-provisioner"

  set {
    name  = "nfs.server"
    value = var.nfs_host
  }

  set {
    name  = "nfs.path"
    value = var.nfs_share_path
  }

  set {
    name  = "storageClass.provisionerName"
    value = "k8s-sigs.io/nfs-subdir-external-provisioner"
  }

  set {
    name  = "storageClass.pathPattern"
    value = var.path_pattern
  }
}
