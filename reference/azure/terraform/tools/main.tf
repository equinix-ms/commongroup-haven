module "certmanager" {
  source = "./modules/helm/cert-manager"

  providers = {
    kubernetes = kubernetes.aks
    helm       = helm.aks
  }

  enable_cert_manager           = var.cert_manager.enabled
  cert_manager_namespace        = var.cert_manager.namespace
  cert_manager_chart_repository = var.cert_manager.chart_repository
  cert_manager_chart_version    = var.cert_manager.chart_version
  cert_manager_settings         = var.cert_manager.settings
}


module "kured" {
  source = "./modules/helm/kured"

  providers = {
    kubernetes = kubernetes.aks
    helm       = helm.aks
  }

  enable_kured           = var.kured.enabled
  kured_namespace        = var.kured.namespace
  kured_chart_repository = var.kured.chart_repository
  kured_chart_version    = var.kured.chart_version
  kured_settings         = var.kured.settings
}


module "fluentd" {
  source = "./modules/helm/fluentd"

  providers = {
    kubernetes = kubernetes.aks
    helm       = helm.aks
  }

  enable_fluentd           = var.fluentd.enabled
  fluentd_namespace        = var.fluentd.namespace
  fluentd_chart_repository = var.fluentd.chart_repository
  fluentd_chart_version    = var.fluentd.chart_version
  fluentd_settings         = var.fluentd.settings
}
